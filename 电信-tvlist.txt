央视,#genre#
CCTV4-国际,http://124.238.110.109:9999/tsfile/live/0004_1.m3u8
CCTV5-体育,http://124.238.110.109:9999/tsfile/live/0005_1.m3u8
CCTV6-电影,http://124.238.110.109:9999/tsfile/live/0006_1.m3u8
CCTV7-军农,http://124.238.110.109:9999/tsfile/live/0007_1.m3u8
CCTV8-电视剧,http://124.238.110.109:9999/tsfile/live/0008_1.m3u8
CCTV9-纪录,http://124.238.110.109:9999/tsfile/live/0009_1.m3u8
CCTV10-科教,http://124.238.110.109:9999/tsfile/live/0010_1.m3u8
CCTV11-戏曲,http://124.238.110.109:9999/tsfile/live/0011_1.m3u8
CCTV12-社会与法,http://124.238.110.109:9999/tsfile/live/0012_1.m3u8
CCTV13-新闻,http://124.238.110.109:9999/tsfile/live/0013_1.m3u8
CCTV14-少儿,http://124.238.110.109:9999/tsfile/live/0014_1.m3u8
CCTV15-音乐,http://124.238.110.109:9999/tsfile/live/0015_1.m3u8
CCTV5+体育赛事,http://124.238.110.109:9999/tsfile/live/0016_1.m3u8
卫视,#genre#
河北卫视,http://124.238.110.109:9999/tsfile/live/0117_1.m3u8
北京卫视高清,http://124.238.110.109:9999/tsfile/live/1011_1.m3u8
湖南卫视高清,http://124.238.110.109:9999/tsfile/live/1012_1.m3u8
黑龙江卫视高清,http://124.238.110.109:9999/tsfile/live/1013_1.m3u8
深圳卫视高清,http://124.238.110.109:9999/tsfile/live/1014_1.m3u8
江苏卫视高清,http://124.238.110.109:9999/tsfile/live/1015_1.m3u8
东方卫视高清,http://124.238.110.109:9999/tsfile/live/1016_1.m3u8
浙江卫视高清,http://124.238.110.109:9999/tsfile/live/1017_1.m3u8
天津卫视高清,http://124.238.110.109:9999/tsfile/live/1018_1.m3u8
安徽卫视,http://124.238.110.109:9999/tsfile/live/1041_1.m3u8
辽宁卫视高清,http://124.238.110.109:9999/tsfile/live/1042_1.m3u8
山东卫视高清,http://124.238.110.109:9999/tsfile/live/1043_1.m3u8
云南卫视,http://124.238.110.109:9999/tsfile/live/1044_1.m3u8
河南卫视,http://124.238.110.109:9999/tsfile/live/1045_1.m3u8
四川卫视高清,http://124.238.110.109:9999/tsfile/live/1059_1.m3u8
江西卫视,http://124.238.110.109:9999/tsfile/live/1060_1.m3u8
贵州卫视,http://124.238.110.109:9999/tsfile/live/1061_1.m3u8
湖北卫视,http://124.238.110.109:9999/tsfile/live/1046_1.m3u8
重庆卫视,http://124.238.110.109:9999/tsfile/live/1048_1.m3u8
东南卫视,http://124.238.110.109:9999/tsfile/live/1049_1.m3u8
广东卫视高清,http://124.238.110.109:9999/tsfile/live/1050_1.m3u8
广西卫视,http://124.238.110.109:9999/tsfile/live/1051_1.m3u8
吉林卫视,http://124.238.110.109:9999/tsfile/live/1052_1.m3u8
陕西卫视,http://124.238.110.109:9999/tsfile/live/1053_1.m3u8
山西卫视,http://124.238.110.109:9999/tsfile/live/1054_1.m3u8
内蒙古卫视,http://124.238.110.109:9999/tsfile/live/1055_1.m3u8
青海卫视,http://124.238.110.109:9999/tsfile/live/1062_1.m3u8
宁夏卫视,http://124.238.110.109:9999/tsfile/live/1063_1.m3u8
西藏卫视,http://124.238.110.109:9999/tsfile/live/1064_1.m3u8
新疆卫视,http://124.238.110.109:9999/tsfile/live/1056_1.m3u8
甘肃卫视,http://124.238.110.109:9999/tsfile/live/1057_1.m3u8
海南卫视,http://124.238.110.109:9999/tsfile/live/1021_1.m3u8
省内,#genre#
河北经济,http://124.238.110.109:9999/tsfile/live/1005_1.m3u8
河北都市,http://124.238.110.109:9999/tsfile/live/1006_1.m3u8
河北影视,http://124.238.110.109:9999/tsfile/live/1007_1.m3u8
河北少儿科教,http://124.238.110.109:9999/tsfile/live/1008_1.m3u8
河北公共,http://124.238.110.109:9999/tsfile/live/1009_1.m3u8
河北农民,http://124.238.110.109:9999/tsfile/live/1010_1.m3u8
沧州公共,http://124.238.110.109:9999/tsfile/live/1023_1.m3u8
沧州新闻,http://124.238.110.109:9999/tsfile/live/1025_1.m3u8
沧州影视,http://124.238.110.109:9999/tsfile/live/1027_1.m3u8
少儿,#genre#
卡酷动画,http://124.238.110.109:9999/tsfile/live/1019_1.m3u8
金鹰卡通,http://124.238.110.109:9999/tsfile/live/1020_1.m3u8
其它,#genre#
世界地理,http://124.238.110.109:9999/tsfile/live/1024_1.m3u8
欢笑剧场,http://124.238.110.109:9999/tsfile/live/1047_1.m3u8
都市剧场,http://124.238.110.109:9999/tsfile/live/1026_1.m3u8
武术世界,http://124.238.110.109:9999/tsfile/live/1000_1.m3u8
央视台球,http://124.238.110.109:9999/tsfile/live/1002_1.m3u8
法治天地,http://124.238.110.109:9999/tsfile/live/1003_1.m3u8
中华美食,http://124.238.110.109:9999/tsfile/live/1004_1.m3u8
新闻综合,http://124.238.110.109:9999/tsfile/live/1030_1.m3u8
天天围棋,http://124.238.110.109:9999/tsfile/live/1031_1.m3u8
金鹰纪实,http://124.238.110.109:9999/tsfile/live/1032_1.m3u8
环球奇观,http://124.238.110.109:9999/tsfile/live/1033_1.m3u8
环球旅游,http://124.238.110.109:9999/tsfile/live/1034_1.m3u8
风云音乐,http://124.238.110.109:9999/tsfile/live/1035_1.m3u8
东奥纪实,http://124.238.110.109:9999/tsfile/live/1022_1.m3u8
梨园频道,http://124.238.110.109:9999/tsfile/live/1036_1.m3u8
快乐垂钓,http://124.238.110.109:9999/tsfile/live/1037_1.m3u8
山东教育,http://124.238.110.109:9999/tsfile/live/1028_1.m3u8
CETV2,http://124.238.110.109:9999/tsfile/live/1029_1.m3u8
CETV4,http://124.238.110.109:9999/tsfile/live/1038_1.m3u8
国防军事,http://124.238.110.109:9999/tsfile/live/1039_1.m3u8
宝贝家,http://124.238.110.109:9999/tsfile/live/1040_1.m3u8
文化精品,http://124.238.110.109:9999/tsfile/live/1058_1.m3u8
文物宝库,http://124.238.110.109:9999/tsfile/live/1065_1.m3u8
老故事,http://124.238.110.109:9999/tsfile/live/1066_1.m3u8
高尔夫网球,http://124.238.110.109:9999/tsfile/live/1067_1.m3u8
纪实高清,http://124.238.110.109:9999/tsfile/live/1068_1.m3u8
电视指南,http://124.238.110.109:9999/tsfile/live/1069_1.m3u8
